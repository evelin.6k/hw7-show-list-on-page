
function showList(array){
    let newArray = array.map(item => `<li>${item}</li>`).join('')
    let list = document.getElementById('list');
    list.innerHTML = newArray;
    return list
}
// showList(["1", "2", "3", "sea", "user", 23]);
